<?php

namespace Nette\Utils;

use Nette,
	Nette\Diagnostics\Debugger;

/**
 * String tools library.
 *
 * @author     Vanek Michal
 */
class Number {

	/**
	 * Static class - cannot be instantiated.
	 */
	final public function __construct() {
		throw new Nette\StaticClassException;
	}

	public static function number($number, $decimals = 0, $dec_point = '.', $thousands_sep = ',') {
		$numbers = explode('.', $number);
		
		if($decimals == 0){
			if(count($numbers) == 1){
				return number_format($number, $decimals, $dec_point, $thousands_sep);
			}else {
				return number_format($number, 2, $dec_point, $thousands_sep);
			}
		}else {
			return number_format($number, $decimals, $dec_point, $thousands_sep);
		}
	}

}
